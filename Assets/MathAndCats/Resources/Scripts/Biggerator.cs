using UnityEngine;
using TMPro;

namespace MathAndCats {

    public class Biggerator : MonoBehaviour {

        Properties props;
        Transform obstacleBucket;
        public int sizeDelta;
        public int colliderDivisor;
        bool triggerProcessed;

        void Awake() {
            props = GetComponent<Properties>();
            obstacleBucket = GameObject.Find("ObstacleBucket").transform;
        }

        void Update() {
            CheckButton();
        }

        void CheckButton() {
            if (props.triggered && !triggerProcessed) {
                triggerProcessed = true;
                UpdateObstacles();
            }
            if (props.idle) {
                triggerProcessed = false;
            }
        }

        void OnTriggerEnter(Collider other) {
            if (other.gameObject.name == "Player") {
                GetComponent<ThingTimer>().TriggerThing();
            }
        }

        void UpdateObstacles() {
            foreach (Transform child in obstacleBucket) {
                child.GetComponent<TextUpdater>().ChangeSize(sizeDelta);
                BoxCollider boxCollider = child.GetComponent<BoxCollider>();
                int colliderDim = sizeDelta / colliderDivisor;
                Vector3 vectorToAdd = new Vector3(colliderDim, colliderDim, colliderDim);
                boxCollider.size = boxCollider.size + vectorToAdd;
            }
        }
    }
}
