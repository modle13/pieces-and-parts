using UnityEngine;
using TMPro;

namespace MathAndCats {
    public class ActionProcessor : MonoBehaviour {

        public virtual void ProcessAction(string action) {
            Debug.Log("Processing the base action, which is nothing; override this in implementation");
        }
    }
}
