﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MainGame {
    public class UIController : MonoBehaviour {

        void Update () {
            if (Input.GetKeyDown("escape")) {
                GameObject elements = transform.Find("UI_elements").gameObject;
                elements.active = !elements.active;
            }
        }

    }
}
