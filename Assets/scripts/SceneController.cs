using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MainGame {

    public class SceneController : MonoBehaviour {

        public string targetScene;

        void OnCollisionEnter(Collision other) {
            Debug.Log(name + " collided with " + other.gameObject.tag);
            if (other.gameObject.tag != "Player") {
                return;
            }
            Debug.Log("hit platform");
            SceneManager.LoadScene (sceneName: targetScene);
        }
    }
}
