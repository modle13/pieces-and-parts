using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MainGame {
    public class MonsterScript : MonoBehaviour {

        private float rotationRecheck = 0.0f;
        private float lastRotationCheck = 1.0f;
        private float max = 5.0f;
        private int positionDivisor = 40;
        private Rigidbody rigidBody;

        void Start() {
            SetCheckTimes();
            rigidBody = GetComponent<Rigidbody>();
        }

        void Update () {
            Vector3 forward = transform.TransformDirection (Vector3.forward);
            transform.position += (new Vector3(forward.x, 0, forward.z)) / positionDivisor;
            if (Time.time - lastRotationCheck < rotationRecheck) {
                return;
            }
            SetCheckTimes();
            Vector3 randomVector = GetRandomVector();
            Vector3 teleportRotation = new Vector3(forward.x + randomVector.x, 0, forward.z + randomVector.z);
            if (teleportRotation != Vector3.zero) {
                transform.rotation = Quaternion.LookRotation(teleportRotation);
            }
        }

        Vector3 GetRandomVector() {
            return new Vector3(GetRandomFloat(), 0, GetRandomFloat());
            // return new Vector3(GetRandomInt(), 0, GetRandomInt());
        }

        int GetRandomInt() {
            return Random.Range(0, 2) * 2 - 1;
        }

        float GetRandomFloat() {
            return Random.Range(-2.0f, 2.0f) * 2.0f - 1.0f;
        }

        void SetCheckTimes() {
            rotationRecheck = Random.Range(0.5f, 2.0f);
            lastRotationCheck = Time.time;
        }

        void OnCollisionEnter(Collision other) {
            if (other.gameObject.tag != "Player") {
                return;
            }
            Debug.Log(name + " says WHYYYYYY");
        }
    }
}
